package com.inxedu.os.edu.dao.impl.websiteimagesdefault;

import com.inxedu.os.common.dao.GenericDaoImpl;
import com.inxedu.os.common.entity.PageEntity;
import com.inxedu.os.edu.dao.websiteimagesdefault.WebsiteImagesDefaultDao;
import com.inxedu.os.edu.entity.websiteimagesdefault.WebsiteImagesDefault;
import org.springframework.stereotype.Repository;

import java.util.List;

/**
 * @author www.inxedu.com
 * @description 模板默认数据（还原用，后期根据方案优化） WebsiteImagesDefaultDao接口实现
 */
@Repository("websiteImagesDefaultDao")
public class WebsiteImagesDefaultDaoImpl extends GenericDaoImpl implements WebsiteImagesDefaultDao{
	/**
     * 添加模板默认数据（还原用，后期根据方案优化）
     */
    public Long addWebsiteImagesDefault(WebsiteImagesDefault websiteImagesDefault){
    	this.insert("WebsiteImagesDefaultMapper.addWebsiteImagesDefault", websiteImagesDefault);
		return websiteImagesDefault.getImageId();
    }
    
    /**
     * 删除模板默认数据（还原用，后期根据方案优化）
     * @param imageId
     */
    public void delWebsiteImagesDefaultByImageId(Long imageId){
    	this.update("WebsiteImagesDefaultMapper.delWebsiteImagesDefaultByImageId", imageId);
    }
    
    /**
     * 修改模板默认数据（还原用，后期根据方案优化）
     * @param websiteImagesDefault
     */
    public void updateWebsiteImagesDefault(WebsiteImagesDefault websiteImagesDefault){
    	this.update("WebsiteImagesDefaultMapper.updateWebsiteImagesDefault", websiteImagesDefault);
    }
    
    /**
     * 通过imageId，查询模板默认数据（还原用，后期根据方案优化）
     * @param imageId
     * @return
     */
    public WebsiteImagesDefault getWebsiteImagesDefaultByImageId(Long imageId){
    	return this.selectOne("WebsiteImagesDefaultMapper.getWebsiteImagesDefaultByImageId", imageId);
    }
    
    /**
     * 分页查询模板默认数据（还原用，后期根据方案优化）列表
     * @param websiteImagesDefault 查询条件
     * @param page 分页条件
     * @return List<WebsiteImagesDefault>
     */
    public List<WebsiteImagesDefault> queryWebsiteImagesDefaultListPage(WebsiteImagesDefault websiteImagesDefault,PageEntity page){
    	return this.queryForListPage("WebsiteImagesDefaultMapper.queryWebsiteImagesDefaultListPage", websiteImagesDefault, page);
    }
    
    /**
     * 条件查询模板默认数据（还原用，后期根据方案优化）列表
     * @param websiteImagesDefault 查询条件
     * @return List<WebsiteImagesDefault>
     */
    public List<WebsiteImagesDefault> queryWebsiteImagesDefaultList(WebsiteImagesDefault websiteImagesDefault){
    	return this.selectList("WebsiteImagesDefaultMapper.queryWebsiteImagesDefaultList", websiteImagesDefault);
    }
}



