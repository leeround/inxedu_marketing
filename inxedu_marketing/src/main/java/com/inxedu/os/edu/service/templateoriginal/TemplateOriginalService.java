package com.inxedu.os.edu.service.templateoriginal;

import java.util.List;

import com.inxedu.os.common.entity.PageEntity;
import com.inxedu.os.edu.entity.templateoriginal.TemplateOriginal;

/**
 * @author www.inxedu.com
 * @description 模板原始文件 TemplateOriginalService接口
 */
public interface TemplateOriginalService{
	/**
     * 添加模板原始文件
     */
    public Long addTemplateOriginal(TemplateOriginal templateOriginal);
    
    /**
     * 删除模板原始文件
     * @param id
     */
    public void delTemplateOriginalById(Long id);
    
    /**
     * 修改模板原始文件
     * @param templateOriginal
     */
    public void updateTemplateOriginal(TemplateOriginal templateOriginal);
    
    /**
     * 通过id，查询模板原始文件
     * @param id
     * @return
     */
    public TemplateOriginal getTemplateOriginalById(Long id);
    
    /**
     * 分页查询模板原始文件列表
     * @param templateOriginal 查询条件
     * @param page 分页条件
     * @return List<TemplateOriginal>
     */
    public List<TemplateOriginal> queryTemplateOriginalListPage(TemplateOriginal templateOriginal, PageEntity page);
    
    /**
     * 条件查询模板原始文件列表
     * @param templateOriginal 查询条件
     * @return List<TemplateOriginal>
     */
    public List<TemplateOriginal> queryTemplateOriginalList(TemplateOriginal templateOriginal);
}



