package com.inxedu.os.edu.constants.enums;
/**
 * 网站管理常量
 * @author www.inxedu.com
 */
public enum WebSiteProfileType {
    web,//网站常规配置
    guide,//导航
    censusCode,//统计代码
    ico,//ico文件
    online,//在线咨询
    template,//模板头尾
    color,//网站颜色
    defaultTemplateId//默认模板页面id
}
