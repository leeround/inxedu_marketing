//初始化方法
$(function(){
	cssNavigation();//设置导航选中样式
	transparentHeaderTempletMove();//透明头部 第一个模板 下移操作

	wmNavFun(); // 手机端导航方法
	cardChangeFun(".news-slide","#n-s-tab li","#n-s-cont section","current",true);
	sSwiperFun(); //幻灯片调取
	couSwiperFun();// 课程分类滚动方法
	d6SwiperFun();// 课程分类滚动方法
	scrollLoad(); //响应滚动加载课程图片
	cardChange(".list-five-box>ul>li",".l-f-in-txt>article");//切换文字
	cardChangeFun(".news-slide","#n-s-tab li","#n-s-cont section","current",true);
	upSlideFun("#iQuestion"); //向上滚动互动
	<!--start 模板七-->
	scrollFunList("upwarp-1");//向上滚动
	<!--end 模板七-->

	jQuery(function ($) {
		var sliderheight=450;
		if($('.mis-box2 .mis-stage').hasClass("stage_height_change")){
			sliderheight=380;
		}
		var slider = $('.mis-box2 .mis-stage').miSlider({
			//  The height of the stage in px. Options: false or positive integer. false = height is calculated using maximum slide heights. Default: false
			stageHeight: sliderheight,
			//  Number of slides visible at one time. Options: false or positive integer. false = Fit as many as possible.  Default: 1
			slidesOnStage: false,
			//  The location of the current slide on the stage. Options: 'left', 'right', 'center'. Defualt: 'left'
			slidePosition: 'center',
			//  The slide to start on. Options: 'beg', 'mid', 'end' or slide number starting at 1 - '1','2','3', etc. Defualt: 'beg'
			slideStart: 'mid',
			//  The relative percentage scaling factor of the current slide - other slides are scaled down. Options: positive number 100 or higher. 100 = No scaling. Defualt: 100
			slideScaling: 150,
			//  The vertical offset of the slide center as a percentage of slide height. Options:  positive or negative number. Neg value = up. Pos value = down. 0 = No offset. Default: 0
			offsetV: -5,
			//  Center slide contents vertically - Boolean. Default: false
			centerV: true,
			//  Opacity of the prev and next button navigation when not transitioning. Options: Number between 0 and 1. 0 (transparent) - 1 (opaque). Default: .5
			navButtonsOpacity: 1
		});
	});
});


$(".d5-us-warp-box li").hover(function(){
	$(this).addClass("hover");
},function(){
	$(this).removeClass("hover");
});
/**
 * 设置导航选中样式
 */
function cssNavigation() {
	var url = window.document.location.pathname;
	$("a[name='" + url + "']").parent().addClass("current");
	if(url=="/"){
		$("#ifCurrent li").first().addClass("current");
	}
}

/**
 * 点击跳转
 * @param url
 * @param newPage
 * @param obj
 */
function getWebpageTemplates(url,newPage,obj) {
	if (self!=top){
		if (url=="/"){
			window.parent.getWebpageTemplates('/index.html',obj)

		}else {
			window.parent.getWebpageTemplates(url,obj)
		}
		window.location.href = url;
		return;
	}
	if (newPage==1){
		window.location.href = url
	}else {
		window.open(url);
	}
}

/**
 * 检查是否手机
 */
function checkIsMobile(){
	var sUserAgent = navigator.userAgent.toLowerCase();
	var bIsIpad = sUserAgent.match(/ipad/i) == "ipad";
	var bIsIphoneOs = sUserAgent.match(/iphone os/i) == "iphone os";
	var bIsMidp = sUserAgent.match(/midp/i) == "midp";
	var bIsUc7 = sUserAgent.match(/rv:1.2.3.4/i) == "rv:1.2.3.4";
	var bIsUc = sUserAgent.match(/ucweb/i) == "ucweb";
	var bIsAndroid = sUserAgent.match(/android/i) == "android";
	var bIsCE = sUserAgent.match(/windows ce/i) == "windows ce";
	var bIsWM = sUserAgent.match(/windows mobile/i) == "windows mobile";
	if (bIsIphoneOs || bIsMidp || bIsUc7 || bIsUc || bIsAndroid || bIsCE || bIsWM) { // 移动端环境下效果
		return true;
	}
	return false;
}


/**
 * 透明模板 下移操作
 *
 * 判断是否背景透明的header
 * header下是否是 banner广告图
 * 头部导航 背景色
 * 第一个模板下移
 */
function transparentHeaderTempletMove(){
	if($("#header").hasClass("n-bg-header")){//是否背景透明的header
		//is_first_banner 所有广告图都添加这个样式
		/*if(!$("#header").parent().parent().next().hasClass("is_first_banner")){//header下是否是 banner广告图*/
		/*如果不是首页就对头部处理、添加class*/
		if (window.location.pathname!="/index.html"&&window.location.pathname!="/"){
			$("#header").parent().parent().parent().parent().removeClass("demo4-main");
			//$("#header").parent().parent().parent().parent().addClass("demo4-main-new");

			//$("#header").css("background-color", "rgba(0,0,0,0.6)");
		}else{
			$("#header").parent().parent().parent().parent().addClass("demo4-main");
			$("#header").css("background-color", "transparent");
		}
	}
	if($("#header").hasClass("op-4-bg")){//
		/*如果不是首页就对头部处理、添加class*/
		if (window.location.pathname!="/index.html"&&window.location.pathname!="/"){
			$("#header").parent().parent().parent().parent().removeClass("demo4-main");
			$("#header").css("background-color", "rgba(0,0,0,0.6)");
			$("#header").parent().parent().parent().parent().addClass("demo4-main-new");
		}else{
			$("#header").parent().parent().parent().parent().addClass("demo4-main");
			//$("#header").css("background-color", "transparent");
		}
	}

	if($("#header").hasClass("op-4-bg")){//是否背景半透明的header
		//is_first_banner 所有广告图都添加这个样式
		/*如果不是首页就对头部处理、添加class*/
		if (window.location.pathname!="/index.html"&&window.location.pathname!="/"){
			if(!$("#header").parent().parent().next().hasClass("is_first_banner")){//header下是否是 banner广告图
				$("#header").parent().parent().parent().parent().removeClass("demo4-main");
			}else{
				$("#header").parent().parent().parent().parent().addClass("demo4-main");
			}
		}
	}
}

$(function(){
	$("a").each(function () {

		if ($(this).attr("href")==''){

			$(this).attr("href","javascript:void(0)");
			$(this).attr("target","_self")
		}

	})


})




