<%@ page language="java" contentType="text/html; charset=UTF-8"%>
<%@ include file="/base.jsp"%>
<%--<div class="nav-wrap">
	<div class="navList">
		<ul>
			<li><div class="line-1" onclick="getWebpageTemplates('/index.html',this)"><a href="javascript:void(0)" title="首页"><img src="/static/admin/assets/home.png" ><span>首页</span></a></div></li>
			<li><div class="line-1" onclick="getWebpageTemplates('/course.html',this)"><a href="javascript:void(0)"  title="课程"><img src="/static/admin/assets/disc.png" ><span>课程</span></a></div></li>
			<li><div class="line-1" onclick="getWebpageTemplates('/article_list.html',this)"><a href="javascript:void(0)"  title="资讯"><img src="/static/admin/assets/zx.png" ><span>资讯</span></a></div></li>
			<li><div class="line-1" onclick="getWebpageTemplates('/teacher.html',this)"><a href="javascript:void(0)"  title="师资"><img src="/static/admin/assets/js.png" ><span>师资</span></a></div></li>
			<li><div class="line-1" onclick="getWebpageTemplates('/about_us.html',this)"><a href="javascript:void(0)"  title="关于我们"><img src="/static/admin/assets/us.png" ><span>关于我们</span></a></div></li>
			&lt;%&ndash;<li><div class="line-1" onclick="getWebpageTemplates('/test.html',this)"><a href="javascript:void(0)"  title="关于我们"><img src="/static/admin/assets/us.png" ><span>测试</span></a></div></li>&ndash;%&gt;
		</ul>
	</div>
	<!-- <a class="prev" title="左" href="javascript: void(0)"> </a>
    <a class="next" title="右" href="javascript: void(0)"> </a> -->
</div>--%>
<script>

	$(function(){
		// 初始化点击第一个一级菜单
		if($(".nav-wrap .navList li a").length>0){
			$(".nav-wrap .navList li a").get(0).click();
		}
	});

	/**
	 * 根据路径获取 所有的 模板 显示在 左侧编辑器
	 * @param webpageUrl
     */
	function getWebpageTemplates(webpageUrl,obj) {
		if($(obj).parent().hasClass("current")){
			return;
		}
		if(isDataChange==false || confirm("刚才的操作，还未发布，确定要离开吗？")){
			//模板 是否 修改 過(排序 ，刪除 ，新增)
			isDataChange=false;//重新加载默认 false
			//头部导航选中
			$(obj).parent().addClass("current").siblings().removeClass("current");

			window.open('/admin/main/index?webPageUrl='+webpageUrl,'mainFrame');
			$("#ui-sMenu").html("");
			$.ajax({
				url:baselocation+'/admin/main/left',
				data:{"webPageUrl":webpageUrl},
				type:'post',
				async:false,
				dataType:'text',
				success:function(result){
					$("#ui-sMenu").html(result);
				}
			});
		}
	}
</script>